# Get list of tasks
curl http://localhost:9000/tasks

# Create a new task
curl -H "Content-Type: application/json" -X POST -d '{
    "description": "Buy some milk(shake)"
}'  http://localhost:9000/tasks

# Update the recently created task
curl -H "Content-Type: application/json" -X PUT -d '{
    "description": "Buy some milk"
}'  http://localhost:9000/tasks/1

# Delete the existing task
curl -X DELETE http://localhost:9000/tasks/1




# registers a new user
curl -H "Content-Type: application/json" -X POST -d '{
    "user": "admin",
    "password": "password"
}' http://localhost:9000/auth/signup

# logs into the application (JWT is generated)
curl -i -H "Content-Type: application/json" -X POST -d '{
    "user": "admin",
    "password": "password"
}' http://localhost:9000/login

# issue a POST request, passing the JWT, to create a task
# remember to replace xxx.yyy.zzz with the JWT retrieved above
curl -H "Content-Type: application/json" \
-H "Authorization: Bearer xxx.yyy.zzz" \
-X POST -d '{
    "description": "Buy watermelon"
}'  http://localhost:9000/tasks

# issue a new GET request, passing the JWT
# remember to replace xxx.yyy.zzz with the JWT retrieved above
curl -H "Authorization: Bearer xxx.yyy.zzz" \
    http://localhost:9000/tasks



        ➜  ~ curl http://localhost:9000/tasks
        {"timestamp":"2018-04-03T21:50:44.269+0000","status":403,"error":"Forbidden","message":"Access Denied","path":"/tasks"}%                                                                                    ➜  ~ curl -H "Content-Type: application/json" -X POST -d '{
            "user": "admin",
            "password": "password"
        }' http://localhost:9000/auth/signup
        {"id":1,"user":"admin","password":"$2a$10$VPtiLVFBZYx9k6EmlhPcSOLJEkkIlsXiNuMfa7rmRDXBmP2hEYcpi"}%                                                                                                          ➜  ~ curl -i -H "Content-Type: application/json" -X POST -d '{
            "user": "admin",
            "password": "password"
        }' http://localhost:9000/login
        HTTP/1.1 200
        Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImV4cCI6MTUyMjc5Mjk1Mn0.E_r8BdK5wBgaTq5_Tyjrx9GoMQmv8h0wm-dup2Y-5FB8ReMVRV8HiLlXN57FF3cIOlXMaP8LWy1pA0yzZBy3Nw
        X-Content-Type-Options: nosniff
        X-XSS-Protection: 1; mode=block
        Cache-Control: no-cache, no-store, max-age=0, must-revalidate
        Pragma: no-cache
        Expires: 0
        X-Frame-Options: DENY
        Content-Length: 0
        Date: Tue, 03 Apr 2018 21:52:32 GMT

        ➜  ~ curl -H "Content-Type: application/json" \
        -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImV4cCI6MTUyMjc5Mjk1Mn0.E_r8BdK5wBgaTq5_Tyjrx9GoMQmv8h0wm-dup2Y-5FB8ReMVRV8HiLlXN57FF3cIOlXMaP8LWy1pA0yzZBy3Nw" \
        -X POST -d '{
            "description": "Buy watermelon"
        }'  http://localhost:9000/tasks
        ➜  ~ curl -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImV4cCI6MTUyMjc5Mjk1Mn0.E_r8BdK5wBgaTq5_Tyjrx9GoMQmv8h0wm-dup2Y-5FB8ReMVRV8HiLlXN57FF3cIOlXMaP8LWy1pA0yzZBy3Nw" \
            http://localhost:9000/tasks
        [{"id":1,"description":"Buy watermelon"}]%
        ➜  ~


docker build --build-arg JAR_FILE="target/spring-jwt-demo-0.0.1-SNAPSHOT.jar" -t spring-jwt-demo:v1 .

        ➜  spring-jwt-demo git:(master) ✗ docker build --build-arg JAR_FILE="target/spring-jwt-demo-0.0.1-SNAPSHOT.jar" -t spring-jwt-demo:v1 .
        Sending build context to Docker daemon  45.82MB
        Step 1/5 : FROM openjdk:8-jdk-alpine
        8-jdk-alpine: Pulling from library/openjdk
        ff3a5c916c92: Pull complete
        5de5f69f42d7: Pull complete
        fd869c8b9b59: Pull complete
        Digest: sha256:e82316151c501a2a0f73b3089da8dc867816470a464fcb191db9f88c2343ad53
        Status: Downloaded newer image for openjdk:8-jdk-alpine
         ---> 224765a6bdbe
        Step 2/5 : VOLUME /tmp
         ---> Running in 01ff08b9dade
        Removing intermediate container 01ff08b9dade
         ---> 0713033fc720
        Step 3/5 : ARG JAR_FILE
         ---> Running in 25d7ed11e748
        Removing intermediate container 25d7ed11e748
         ---> b755eb213e65
        Step 4/5 : ADD ${JAR_FILE} app.jar
         ---> b46ead0b87c1
        Step 5/5 : ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
         ---> Running in d1ad8e44a657
        Removing intermediate container d1ad8e44a657
         ---> be505ed9e761
        Successfully built be505ed9e761
        Successfully tagged spring-jwt-demo:v1
        ➜  spring-jwt-demo git:(master) ✗

docker run -p 9000:9000 --name spring-jwt-demo  spring-jwt-demo:v1 -t

with debug port.
docker run -p 9000:9000 -p 9001:9001 --name spring-jwt-demo  spring-jwt-demo:v1 -t