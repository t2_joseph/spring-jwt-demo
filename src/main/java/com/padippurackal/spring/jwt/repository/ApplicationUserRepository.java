package com.padippurackal.spring.jwt.repository;

import com.padippurackal.spring.jwt.user.ApplicationUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ApplicationUserRepository extends JpaRepository<ApplicationUser, Long> {

    ApplicationUser findByUser(String user);

}
