package com.padippurackal.spring.jwt.user;

import com.padippurackal.spring.jwt.repository.ApplicationUserRepository;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    ApplicationUserRepository applicationUserRepository;

    public UserDetailsServiceImpl(ApplicationUserRepository applicationUserRepository){
        this.applicationUserRepository = applicationUserRepository;
    }
    @Override
    public UserDetails loadUserByUsername(String user) throws UsernameNotFoundException {

        ApplicationUser applicationUser = applicationUserRepository.findByUser(user);
        if (applicationUser == null) {
            throw new UsernameNotFoundException(user);
        }

        return new User(applicationUser.getUser(), applicationUser.getPassword(), new ArrayList<>());
    }
}
