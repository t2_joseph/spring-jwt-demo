package com.padippurackal.spring.jwt.constants;

public class SecurityConstants {
    public static final String AUTH_HEADER = "Authorization";
    public static final String TOKEN_PREFIX = "Bearer ";

    public static final long EXPIRATION_TIME = 600000L;
    public static final String SECRET = "SecretKey";

    public static final String SIGN_UP_URL = "/auth/signup";

}
